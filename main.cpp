#include <iostream> //Wczytanie zależności
#include <climits>

using namespace std; //Używamy przestrzeni nazw std. Nie występują w kodzie żadne kolizje nazw.

/*
* Główna funkcja inicjalizująca program
*/
int main() {
	int n = 0;
	int i = 0, min = INT_MAX, input;

	//Wyznaczanie n
	cout << "Podaj n: ";
	cin >> n;
	cout << endl;

	//Jeśli n <= 0 - informacja o błędzie i zakończenie programu
	if(n <= 0) {
		cout << "N musi byc wieksze od 0" << endl;
		return 1;
	}

	int *array = new int [n]; //Utworzenie tablicy

	//przypisywanie kolejnych wartości i wyznaczenie wartosci minimalnej
	while(i < n) {
		cin >> input;
		if(input < min) {
			min = input;
		}
		i++;
	}

	cout << "Najmniejsza wartosc: " << min << endl; //Wypisanie wartosci minimalnej

	//przypisywanie kolejnych wartości jeszcze raz i wyznaczenie wartosci minimalnej
	i = 0;
	min = INT_MAX;
	while(i < n) {
		cin >> array[i];
		if(array[i] < min) {
			min = array[i];
		}
		i++;
	}

	//Wypisanie różnicy
	for(i = 0; i < n; i++) {
		cout << "Roznica miedzy elementem " << (i + 1) << " o wartosci " << array[i] << " a wartoscia najmniejsza tablicy wynosi " << array[i] - min << endl;
	}

	return 0;
}